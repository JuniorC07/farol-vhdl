LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY farol IS

PORT ( clock, ped, reset   : IN BIT; 
		 lite					   : BUFFER INTEGER RANGE 0 TO 7;
		 change					: BUFFER BIT;
		 pedred, pedgrn : OUT BIT;
		 mainred, mainyelo, maingrn : OUT BIT;
		 sidered, sideyelo, sidegrn : OUT BIT);
		 
END farol;

ARCHITECTURE toplevel OF farol IS
COMPONENT delay
  PORT  ( clock, reset			: IN BIT;
	       lite 							: IN INTEGER RANGE 0 TO 7;
			 change							: OUT BIT);
			 
END COMPONENT;

COMPONENT control
   PORT ( clock, enable, reset, ped		: IN BIT;
			 lite								: OUT INTEGER RANGE 0 TO 7);

END COMPONENT;

COMPONENT lite_ctrl
   PORT ( lite									: IN INTEGER RANGE 0 TO 7;
			 mainred, mainyelo, maingrn	: OUT BIT;
			 pedred, pedgrn : OUT BIT;
			 sidered, sideyelo, sidegrn	: OUT BIT);
			 
END COMPONENT;

BEGIN

module1: delay		 PORT MAP (clock => clock, reset => reset,
									 lite => lite, change => change);

module2: control	 PORT MAP (clock => clock, enable => change,
									 reset => reset, lite => lite, ped => ped);
									 
module3: lite_ctrl PORT MAP (lite => lite, mainred => mainred, mainyelo => mainyelo,
									  maingrn => maingrn, sidered => sidered,
									  sideyelo => sideyelo, sidegrn => sidegrn, pedred => pedred, pedgrn => pedgrn);

END toplevel;

----------------------------------------------------------------------------------

ENTITY delay IS

PORT ( clock, reset			: IN BIT;
		 lite								: IN BIT_VECTOR (2 DOWNTO 0);
		 change							: OUT BIT);
		 
END delay;


ARCHITECTURE time OF delay IS

BEGIN
	PROCESS (clock, reset)
	VARIABLE mach					: INTEGER RANGE 0 TO 31;
	BEGIN
	IF reset = '0' THEN mach := 0;
	
	ELSIF (clock = '1' AND clock'EVENT) THEN  -- com clock de 1 Hz, tempos em segundos

		IF mach = 0 THEN
			CASE lite IS 					 -- Seta o temporizador para 3 para todos os casos
				WHEN "000" => mach := 3; 
				WHEN "001" => mach := 3;       
				WHEN "010" => mach := 3;  
				WHEN "011" => mach := 3;        
				WHEN "100" => mach := 3;         
				WHEN OTHERS => mach := 1; -- Caso receba lite receba um valor inesperado, seta o change como 1
			END CASE;
		
		ELSE mach := mach - 1;          -- decrementa temporizador
		END IF;
	
	END IF;
	
	IF mach = 1 THEN change <= '1';    -- se o temporizador é igual a 1, muda o change para 1
	ELSE change <= '0';
	END IF;
	
	END PROCESS;

END time;

-------------------------------------------------------------------

ENTITY control IS
PORT ( clock, enable, reset, ped : IN BIT;  -- portas definidas conforme definição nos módulos(linha 39 a 46)
		 lite : OUT BIT_VECTOR (2 DOWNTO 0)); -- onde o enable recebe o valor de change do módulo anterior

END control;

ARCHITECTURE a OF control IS
TYPE enumerated IS (mgrn, myel, sgrn, syel, pedgrn);  -- Enumeração com os 5 possíveis estados de aula

BEGIN
	PROCESS (clock, reset)
	VARIABLE lights :enumerated;
	BEGIN
		
		IF reset = '0' THEN lights := mgrn;
		ELSIF (clock = '1' AND clock'EVENT) THEN
			IF enable = '1' THEN    
				IF ped =  '1' THEN 	-- caso o botão de pedestre estiver ativo  seta o ciclo com uma condição a mais, mudando do segundo amarelo para verde p/pedestres
					CASE lights IS
						WHEN mgrn => lights := myel;
						WHEN myel => lights := sgrn;
						WHEN sgrn => lights := syel;
						WHEN syel => lights := pedgrn;
						WHEN pedgrn => lights := mgrn;
					END CASE;
				ELSE  
					CASE lights IS  --ciclo normal, sem a condição adicional(mantém sempre vermelho para os pedestres)
						WHEN mgrn => lights := myel;
						WHEN myel => lights := sgrn;
						WHEN sgrn => lights := syel;
						WHEN syel => lights := mgrn;
						WHEN pedgrn => lights := mgrn;
					END CASE;
				END IF;
			END IF;
		END IF;
		
		CASE lights IS           -- padrões para os estados da luz
			WHEN mgrn=> lite <="000";
			WHEN myel=> lite <="001";
			WHEN sgrn=> lite <= "010";
			WHEN syel=> lite <= "011";
			WHEN pedgrn => lite <= "100";
		END CASE;
	END PROCESS;
END a;

-----------------------------------------------------------

ENTITY lite_ctrl IS

PORT ( lite :IN BIT_VECTOR (2 DOWNTO 0);

				mainred, mainyelo, maingrn, pedgrn, pedred   :OUT BIT;
            sidered, sideyelo, sidegrn   :OUT BIT);
END lite_ctrl;

ARCHITECTURE patterns OF lite_ctrl IS
BEGIN
	
	PROCESS (lite)
BEGIN
CASE lite IS  -- responsável por definir quais luzes devem ficar acesas
  WHEN "000" => maingrn <= '1'; mainyelo <= '0'; mainred <= '0';
               sidegrn <= '0'; sideyelo <= '0'; sidered <= '1';
					pedgrn <= '0'; pedred <= '1';
  WHEN "001" => maingrn <= '0'; mainyelo <= '1'; mainred <= '0';
               sidegrn <= '0'; sideyelo <= '0'; sidered <= '1';
					pedgrn <= '0'; pedred <= '1';
  WHEN "010" => maingrn <= '0'; mainyelo <= '0'; mainred <= '1';
               sidegrn <= '1'; sideyelo <= '0'; sidered <= '0';
					pedgrn <= '0'; pedred <= '1';
  WHEN "011" => maingrn <= '0'; mainyelo <= '0'; mainred <= '1';
               sidegrn <= '0'; sideyelo <= '1'; sidered <= '0'; 
					pedgrn <= '0'; pedred <= '1';
  WHEN "100" => maingrn <= '0'; mainyelo <= '0'; mainred <= '1';
               sidegrn <= '0'; sideyelo <= '0'; sidered <= '1';
					pedgrn <= '1'; pedred <= '0';
  WHEN OTHERS => maingrn <= '0'; mainyelo <= '0'; mainred <= '0';
               sidegrn <= '0'; sideyelo <= '0'; sidered <= '0';
					pedgrn <= '0'; pedred <= '0';
					
END CASE;
END PROCESS;
END patterns;
	 

-----------