-- Copyright (C) 2017  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "10/19/2021 21:18:50"
                                                            
-- Vhdl Test Bench template for design  :  farol_transito
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY farol_transito_vhd_tst IS
END farol_transito_vhd_tst;
ARCHITECTURE farol_transito_arch OF farol_transito_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL altera_estado : STD_LOGIC;
SIGNAL clock_entrada : STD_LOGIC;
SIGNAL entrada_reset : STD_LOGIC;
SIGNAL entrada_sensor : STD_LOGIC;
SIGNAL estados : STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL pessoa_verde : STD_LOGIC;
SIGNAL pessoa_vermelha : STD_LOGIC;
SIGNAL principal_amarela : STD_LOGIC;
SIGNAL principal_verde : STD_LOGIC;
SIGNAL principal_vermelha : STD_LOGIC;
SIGNAL secundaria_amarela : STD_LOGIC;
SIGNAL secundaria_verde : STD_LOGIC;
SIGNAL secundaria_vermelha : STD_LOGIC;
COMPONENT farol_transito
	PORT (
	altera_estado : OUT STD_LOGIC;
	clock_entrada : IN STD_LOGIC;
	entrada_reset : IN STD_LOGIC;
	entrada_sensor : IN STD_LOGIC;
	estados : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
	pessoa_verde : OUT STD_LOGIC;
	pessoa_vermelha : OUT STD_LOGIC;
	principal_amarela : OUT STD_LOGIC;
	principal_verde : OUT STD_LOGIC;
	principal_vermelha : OUT STD_LOGIC;
	secundaria_amarela : OUT STD_LOGIC;
	secundaria_verde : OUT STD_LOGIC;
	secundaria_vermelha : OUT STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : farol_transito
	PORT MAP (
-- list connections between master ports and signals
	altera_estado => altera_estado,
	clock_entrada => clock_entrada,
	entrada_reset => entrada_reset,
	entrada_sensor => entrada_sensor,
	estados => estados,
	pessoa_verde => pessoa_verde,
	pessoa_vermelha => pessoa_vermelha,
	principal_amarela => principal_amarela,
	principal_verde => principal_verde,
	principal_vermelha => principal_vermelha,
	secundaria_amarela => secundaria_amarela,
	secundaria_verde => secundaria_verde,
	secundaria_vermelha => secundaria_vermelha
	);
init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once                      
WAIT;                                                       
END PROCESS init;                                           
always : PROCESS                                              
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN                                                         
	clock_entrada <= '0', '1' AFTER 5 NS;
	entrada_reset <= '1';
	entrada_sensor <= '0';
	WAIT FOR 10 NS;                                                     
END PROCESS always;                                          
END farol_transito_arch;
