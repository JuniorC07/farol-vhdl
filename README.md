# Farol - VHDL



Implementar um semáforo com duas vias e com passagem de pedestre.
Lógica de funcionamento do sistema:
-	O semáforo deverá funcionar normalmente (sem sensor), permitindo, na sequência, a passagem de carros pelas ruas. Caso houver pedestres, este deverá apertar um botão para, então aguardar a sua vez para atravessar a rua.
-	O sistema permitirá o ajuste de tempo somente via software (firmware), sem possibilidade de ajuste através de nenhum sistema externo (sensor, chave, etc).
